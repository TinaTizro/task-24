CREATE DATABASE ´journal´;
USE ´Journal´;

CREATE TABLE author (
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
first_name VARCHAR (80) NOT NULL,
last_name VARCHAR (128) NOT NULL,
created_at DATETIME NOT NULL DEFAULT NOW(),
update_at DATETIME DEFAULT NULL
);

CREATE TABLE journal (
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR (80) NOT NULL,
created_at DATETIME NOT NULL DEFAULT NOW() NOT NULL,
update_at DATETIME DEFAULT NULL
);
CREATE TABLE journal_entry (
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
title VARCHAR (128) NOT NULL,
`text`TEXT,
created_at DATETIME NOT NULL DEFAULT NOW(),
update_at DATETIME DEFAULT NULL,
author_id INT NOT NULL,
journal_id INT NOT NULL,
FOREIGN KEY (author_id) REFERENCES author(id),
FOREIGN KEY (journal_id) REFERENCES journal(id)
);

CREATE TABLE `tags` ( 
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
title VARCHAR (80) NOT NULL,
created_at DATETIME NOT NULL DEFAULT NOW() NOT NULL,
update_at DATETIME DEFAULT NULL
);

CREATE TABLE journal_tag (
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
tag_id INT,
journal_entry_id INT,
FOREIGN KEY (tag_id) REFERENCES tag(id),
FOREIGN KEY (journal_entry_id) REFERENCES journal_entry(id)
);




