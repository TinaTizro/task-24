SELECT * FROM ´journal´.journal_entry 
INNER JOIN author ON author.id = author_id
INNER JOIN journal ON journal.id = journal_id;

SELECT journal_entry.title, journal_entry.text, CONCAT(author.first_name,' ', author.last_name) AS author_name, journal.name, GROUP_CONCAT(tag.title) AS tags
FROM ´journal´.journal_entry 
INNER JOIN author ON author.id = author_id
INNER JOIN journal ON journal.id = journal_id
LEFT JOIN journal_tag ON journal_tag.journal_entry_id = journal_entry.id
LEFT JOIN tag ON journal_tag.tag_id = tag.id
GROUP BY journal_entry.id;

